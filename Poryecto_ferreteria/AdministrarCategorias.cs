﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Poryecto_ferreteria
{
    public partial class AdminCategorias : Form
    {
        Categorias categoria = new Categorias();
        BRINDISEntities bd = new BRINDISEntities();
        public AdminCategorias()
        {
            InitializeComponent();
        }

        private void MntNuevoCategoria_Click(object sender, EventArgs e)
        {
           
            ctDescripcionCategoria.Enabled = true;
            MntGuardarCategoria.Enabled = true;
            int max = bd.Categorias.Select(p => p.Cate_clave).DefaultIfEmpty(0).Max();
            limpiar();
            ctClaveCategoria.Enabled = false;
            max = max + 1;
            ctClaveCategoria.Text = max.ToString();
            MntEliminarCategoria.Enabled = true;
        }
        private void MntEliminarCategoria_Click(object sender, EventArgs e)
        {
            Eliminar();
            ActualizarLista();
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.Hide();
            Pantalla_principal pantalla_principal = new Pantalla_principal();
            pantalla_principal.Show(this);
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        void limpiar() 
        {
            ctClaveCategoria.Text = "";
            ctDescripcionCategoria.Text = "";
        }
        void Guardar()
        {
            int max = bd.Categorias.Select(p => p.Cate_clave).DefaultIfEmpty(0).Max();
            if (!ValidarCajasDeTexto())
            {
                return;
            }
            if (max >= Convert.ToInt32( ctClaveCategoria.Text))
            {
                Actualizar();
            }
            else 
            {
                categoria.Cate_clave = Convert.ToInt32(ctClaveCategoria.Text.Trim());
                categoria.Cate_Descripcion = ctDescripcionCategoria.Text;
                bd.Categorias.Add(categoria);
                bd.SaveChanges();
                MessageBox.Show("La categoria: "+categoria.Cate_Descripcion+" se guardo correctamente");
                buscar(categoria.Cate_Descripcion);
            }

        }
        void Eliminar() 
        {
            try
            {
                categoria.Cate_clave = Convert.ToInt32(ctClaveCategoria.Text);
                categoria.Cate_Descripcion = ctDescripcionCategoria.Text;
                CambiarCategoriaEliminada(categoria.Cate_clave);
                bd.Categorias.Remove(categoria);
                bd.SaveChanges();
                limpiar();
            }
            catch (Exception e)
            {

                MessageBox.Show(e.Message);
            }

        }
        void CambiarCategoriaEliminada(int idCategoria) 
        {
            string mensaje = "";
            DialogResult result = MessageBox.Show("Los clientes con esta categoria seran modificados", "Seguro que quiere borrar esta categoria",MessageBoxButtons.YesNo,MessageBoxIcon.Warning);
            if (result == DialogResult.No)
            {
                return;
            }
            else if(result == DialogResult.Yes)
            {

                List<Clientes> clientesModificados = new List<Clientes>();
                foreach (var cliente in bd.Clientes)
                {
                    if (cliente.Cli_categotia_id == idCategoria)
                    {
                        cliente.Cli_categotia_id = 0;
                        clientesModificados.Add(cliente);
                    }
                }

                foreach (var clienteModificado in clientesModificados)
                {
                    bd.Clientes.Remove(clienteModificado);
                }
                bd.SaveChanges();
                foreach (var clienteModificado in clientesModificados)
                {
                    bd.Clientes.Add(clienteModificado);
                }
                bd.SaveChanges();

                MessageBox.Show("Los clientes con esa categoria se modificado a 'Sin categoria'", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                result = MessageBox.Show("Quiere ver los clientes modificados", "Revicion", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    if (clientesModificados.Count == 0)
                    {
                        MessageBox.Show("No se modifico ningun cliente");
                        return;
                    }
                    foreach (var c in clientesModificados)
                    {
                        mensaje = mensaje + c.Cli_nombre;
                    }
                    MessageBox.Show(mensaje, "Lista de clientes modificados", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }



        }
        void Actualizar() 
        {
            categoria.Cate_clave = Convert.ToInt32(ctClaveCategoria.Text.Trim());
            categoria.Cate_Descripcion = ctDescripcionCategoria.Text.Trim();
            ActualizarCliente(categoria.Cate_clave, categoria.Cate_Descripcion);
            MessageBox.Show("Categoria actualiza correctamente");

        }
        void ActualizarCliente(int clave, string NuevaDescripcion) 
        {
            DialogResult result = MessageBox.Show("si existen clientes con esta categoria seran modificados", "Seguro que quiere actualizar esta categoria", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (result == DialogResult.No)
            {
                return;
            }
            List<Clientes> clientesModificados = new List<Clientes>();
            foreach (var cliente in bd.Clientes)
            {
                if (cliente.Cli_categotia_id == Convert.ToInt32(ctClaveCategoria.Text))
                {
                    clientesModificados.Add(cliente);
                }
            }
            if (clientesModificados.Count == 0)
            {
                categoria.Cate_clave = clave;
                bd.Categorias.Remove(categoria);
                bd.SaveChanges();
                categoria.Cate_clave = clave;
                categoria.Cate_Descripcion = NuevaDescripcion;
                bd.Categorias.Add(categoria);
                bd.SaveChanges();
            }
            foreach (var clientes in clientesModificados)
            {
                bd.Clientes.Remove(clientes);

            }
            bd.SaveChanges();
            categoria.Cate_clave = clave;
            bd.Categorias.Remove(categoria);
            bd.SaveChanges();
            categoria.Cate_clave = clave;
            categoria.Cate_Descripcion = NuevaDescripcion;
            bd.Categorias.Add(categoria);
            bd.SaveChanges();
            foreach (var c in clientesModificados)
            {
                c.Cli_categotia_id = clave;
                bd.Clientes.Add(c);
            }
            bd.SaveChanges();



        }
        private void buscarToolStripMenuItem_Click(object sender, EventArgs e)
        {

            buscar(ctMBuscar.Text.Trim());
        }
        private void MntGuardarCategoria_Click(object sender, EventArgs e)
        {
            Guardar();
            ActualizarLista();
        }

        bool ValidarCajasDeTexto() 
        {
            bool a = true;
            if (ctDescripcionCategoria.Text == "")
            {
                ctDescripcionCategoria.Text = "Porfavor de agregar una descripcion";
                a = false;
            }
            return a;
        }

        private void AdminCategorias_Load(object sender, EventArgs e)
        {
            MntEliminarCategoria.Enabled = false;
            MntGuardarCategoria.Enabled = false;
            ctClaveCategoria.Enabled = false;
            llenarLista();

        }

        private void lbCategorias_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbCategorias.SelectedItem == null)
            {
                return;
            }
            if (lbCategorias.SelectedItem.ToString().Trim() != "Sin categoria" )
            {
                buscar(lbCategorias.SelectedItem.ToString().Trim());

            }
        }

        private void ctDescripcionCategoria_TextChanged(object sender, EventArgs e)
        {
            if (ctDescripcionCategoria.Text == "Porfavor de agregar una descripcion")
            {
                ctDescripcionCategoria.Text = "";
            }
        }
        void buscar(string desc) 
        {
            categoria = bd.Categorias.FirstOrDefault(x => x.Cate_Descripcion == desc);
            if (categoria == null)
            {
                MessageBox.Show("No existe esa categoria");
                return;
            }
            ctClaveCategoria.Text = categoria.Cate_clave.ToString();
            ctDescripcionCategoria.Text = categoria.Cate_Descripcion.Trim();
            ctClaveCategoria.Enabled = false;
            MntEliminarCategoria.Enabled = true;
            MntGuardarCategoria.Enabled = true;
        }
        void ActualizarLista() 
        {
            lbCategorias.Items.Clear();
            llenarLista();

        }
        void llenarLista() 
        {
            foreach (var cat in bd.Categorias)
            {
                lbCategorias.Items.Add(cat.Cate_Descripcion);
            }
        }
        
    }
}
