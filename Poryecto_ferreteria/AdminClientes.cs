﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Poryecto_ferreteria
{
    public partial class AdminClientes : Form
    {
        Clientes Cliente = new Clientes();
        Categorias Categoria = new Categorias();
        BRINDISEntities bd = new BRINDISEntities();
        public AdminClientes()
        {
            InitializeComponent();
        }
        private void AdminClientes_Load(object sender, EventArgs e)
        {
            MntGuardarClientes.Enabled = false;
            MntEliminarClientes.Enabled = false;
            llenarListaClientes();
            ctMBuscarCliente.Focus();
            ctClaveCliente.Enabled = false;
            foreach (var categorias in bd.Categorias)
            {
                cbCategoriaCliente.Items.Add(categorias.Cate_Descripcion.Trim());
                cbFiltros.Items.Add(categorias.Cate_Descripcion.Trim());
            }

        }
        private void MntNuevoClientes_Click(object sender, EventArgs e)
        {
            MntGuardarClientes.Enabled = true;
            MntEliminarClientes.Enabled = false;
            int maxAge = bd.Clientes.Select(p => p.Cli_clave).DefaultIfEmpty(0).Max();
            maxAge = maxAge + 1;
            Limpiar();
            ctClaveCliente.Text = maxAge.ToString();
        }
        private void btnAtras_Click(object sender, EventArgs e)
        {
            Pantalla_principal Pantalla_principal = new Pantalla_principal();
            if (RevisarSiHayDatos() == true)
            {
                DialogResult result = MessageBox.Show("Seguro que quieres regresar se perderan los datos añadidos", "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {

                    Pantalla_principal.Show();
                    this.Hide();
                }
                if (result == DialogResult.No)
                {
                    return;
                }
            }
            Pantalla_principal.Show();
            this.Hide();


        }
        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void MntGuardarClientes_Click(object sender, EventArgs e)
        {
            ctMBuscarCliente.Focus();
            int maxAge = bd.Clientes.Select(p => p.Cli_clave).DefaultIfEmpty(0).Max();
            
            try
            {
                if (!ValidarCajasDeTexto())
                {
                    return;
                }
                if (Convert.ToInt32(ctClaveCliente.Text) > maxAge)
                {
                    GuardarCliente();
                    borrarDatosLista();
                    llenarListaClientes();

                }
                else
                {
                    Actualizar();
                    borrarDatosLista();
                    llenarListaClientes();
                }
            }
            catch (Exception a)
            {

                MessageBox.Show(a.Message);
            }


        }
        private void buscarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            buscar(ctMBuscarCliente.Text.Trim());
        }
        bool RevisarSiHayDatos()
        {
            if (ctClaveCliente.Text != "")
            {
                return true;
            }
            if (ctDireccionCliente.Text != "")
            {
                return true;
            }
            if (ctNombreCliente.Text != "")
            {
                return true;
            }
            if (ctRfc.Text != "")
            {
                return true;

            }
            if (cbCategoriaCliente.Text != "")
            {
                return true;
            }
            return false;
        }
        void GuardarCliente()
        {
            if (ctClaveCliente.Text =="")
            {
                return;
            }
            if (RevisarSiHayDatos())
            {
                Cliente.Cli_clave = Convert.ToInt32(ctClaveCliente.Text.Trim());
                Cliente.Cli_nombre = ctNombreCliente.Text.Trim();
                Cliente.Cli_direccion = ctDireccionCliente.Text.Trim();
                Cliente.Cli_fechaNacimiento = dtpFechaNacimientoCliente.Value;
                Cliente.Cli_rfc = ctRfc.Text.Trim();
                if (cbCategoriaCliente.Text == "")
                {
                    Cliente.Cli_categotia_id = 0;
                }
                else
                {
                    Categoria = bd.Categorias.FirstOrDefault(x => x.Cate_Descripcion == cbCategoriaCliente.Text);
                    Cliente.Cli_categotia_id = Categoria.Cate_clave;
                }

                try
                {
                    bd.Clientes.Add(Cliente);
                    bd.SaveChanges();
                    MessageBox.Show("Usuario guardado correctamente");
                }
                catch (Exception e)
                {

                    MessageBox.Show(Convert.ToString("Error de conexion: " + e));
                }


            }
        }
        void Actualizar() 
        {
            if (RevisarSiHayDatos())
            {
                Cliente.Cli_clave = Convert.ToInt32(ctClaveCliente.Text.Trim());
                Cliente.Cli_nombre = ctNombreCliente.Text.Trim();
                Cliente.Cli_direccion = ctDireccionCliente.Text.Trim();
                Cliente.Cli_fechaNacimiento = dtpFechaNacimientoCliente.Value;
                Categoria = bd.Categorias.FirstOrDefault(x => x.Cate_Descripcion == cbCategoriaCliente.Text);
                Categoria.Cate_Descripcion.Trim();
                Cliente.Cli_categotia_id = Categoria.Cate_clave;
                Cliente.Cli_rfc = ctRfc.Text.Trim();
                try
                {
                    bd.Clientes.Remove(Cliente);
                    bd.SaveChanges();
                    bd.Clientes.Add(Cliente);
                    bd.SaveChanges();
                    MessageBox.Show("Usuario actualizado correctamente");
                }
                catch (Exception e)
                {

                    MessageBox.Show(Convert.ToString("Error de conexion: " + e));
                }


            }
        }
        void Limpiar()
        {
            ctClaveCliente.Text = "";
            ctDireccionCliente.Text = "";
            ctNombreCliente.Text = "";
            ctRfc.Text = "";
            dtpFechaNacimientoCliente.Value = DateTime.Now;
            cbCategoriaCliente.Text = "";
        }
        private void MntEliminarClientes_Click(object sender, EventArgs e)
        {
            if (RevisarSiHayDatos())
            {
                Cliente.Cli_clave = Convert.ToInt32(ctClaveCliente.Text.Trim());
                Cliente.Cli_nombre = ctNombreCliente.Text.Trim();
                Cliente.Cli_direccion = ctDireccionCliente.Text.Trim();
                Cliente.Cli_fechaNacimiento = dtpFechaNacimientoCliente.Value;
                Categoria = bd.Categorias.FirstOrDefault(x => x.Cate_Descripcion == cbCategoriaCliente.Text);
                Cliente.Cli_categotia_id = Categoria.Cate_clave;
                try
                {
                    bd.Clientes.Remove(Cliente);
                    bd.SaveChanges();
                    MessageBox.Show("Usuario borrado correctamente");
                    Limpiar();
                    borrarDatosLista();
                    llenarListaClientes();
                }
                catch (Exception o)
                {

                    MessageBox.Show(Convert.ToString("Error de conexion: " + o));
                }


            }
        }
        bool ValidarCajasDeTexto() 
        {
            bool a = false, b = false, c = false, d = false, f = false;
            if (ctDireccionCliente.Text == "")
            {
                a = true;
            }
            if (ctNombreCliente.Text == "")
            {
                b = true;
            }
            if (ctRfc.Text == "")
            {
                c = true;
            }
            if (cbCategoriaCliente.Text == "")
            {
                d = true;
            }
            if (!ValidarSiNohayCajasVasias(a,b,c,d))
            {
                return f;
            }
            return true;
            
        }
        bool ValidarSiNohayCajasVasias(bool a, bool b, bool c, bool d) 
        {
            bool res = true;
            if (a)
            {
                ctDireccionCliente.Text = "Porfavor de llenar este dato";
                res = false;
            }
            if (c)
            {
                ctRfc.Text = "Porfavor llenar este campo";
                res = false;
            }
            if (b)
            {
                ctNombreCliente.Text = "Porfavor llenar este campo";
                res = false;
            }
            if (d)
            {
                MessageBox.Show("Porfavor eligir una categoria (en caso de necesitarse)");
            }
            return res;
        }

        private void ctRfc_TextChanged(object sender, EventArgs e)
        {
            Focus();
        }

        private void ctRfc_Click(object sender, EventArgs e)
        {
            if (ctRfc.Text == "Porfavor llenar este campo")
            {
                ctRfc.Text = "";
            } 
        }

        private void ctNombreCliente_TextChanged(object sender, EventArgs e)
        {
            if (ctNombreCliente.Text == "Porfavor llenar este campo")
            {
                ctNombreCliente.Text = "";
            }
        }

        private void ctDireccionCliente_TextChanged_1(object sender, EventArgs e)
        {
            if (ctDireccionCliente.Text == "Porfavor llenar este campo")
            {
                ctDireccionCliente.Text = "";
            }
        }

        void llenarListaClientes() 
        {
            if (cbFiltros.Text == "")
            {
                foreach (var cli in bd.Clientes)
                {
                    lbClientes.Items.Add(cli.Cli_nombre);
                }
            }
            else
            {
                foreach (var cli in BuscarPorCategoriaSelecionada(cbFiltros.Text.Trim()))
                {
                    lbClientes.Items.Add(cli.Cli_nombre);
                }
            }




        }
        List<Clientes> BuscarPorCategoriaSelecionada(string categoria) 
        {
            List<Clientes> clientesFiltrados = new List<Clientes>();
            var clientes = bd.Clientes;
            var cat = bd.Categorias.FirstOrDefault(x=>x.Cate_Descripcion == categoria);
            foreach (var cli in clientes)
                {
                    if (cli.Cli_categotia_id == cat.Cate_clave)
                    {
                    clientesFiltrados.Add(cli);
                    }
                }
            return clientesFiltrados;
        }

        private void cbFiltros_SelectedIndexChanged(object sender, EventArgs e)
        {
            borrarDatosLista();
            llenarListaClientes();
        }
        void borrarDatosLista() 
        {
            lbClientes.Items.Clear();
        }

        private void lbClientes_SelectedIndexChanged(object sender, EventArgs e)
        {
            MntGuardarClientes.Enabled = true;
            MntEliminarClientes.Enabled = true;
            if (lbClientes.SelectedItem == "" || lbClientes.SelectedItem == null)
            {
                return;
            }
            buscar(lbClientes.SelectedItem.ToString());
        }
        void buscar(string nombreUsu) 
        {
            if (nombreUsu == "")
            {
                return;
            }
            MntGuardarClientes.Enabled = true;
            Cliente = bd.Clientes.FirstOrDefault(x => x.Cli_nombre == nombreUsu);
            if (Cliente != null)
            {
                Categoria = bd.Categorias.Find(Cliente.Cli_categotia_id);
                ctClaveCliente.Enabled = false;
                ctClaveCliente.Text = Cliente.Cli_clave.ToString();
                ctDireccionCliente.Text = Cliente.Cli_direccion.Trim();
                ctNombreCliente.Text = Cliente.Cli_nombre.Trim();
                ctRfc.Text = Cliente.Cli_rfc.Trim();
                dtpFechaNacimientoCliente.Value = Cliente.Cli_fechaNacimiento.Value;
                cbCategoriaCliente.Text = Categoria.Cate_Descripcion.Trim();
                MntEliminarClientes.Enabled = true;
            }
            else
            {
                MessageBox.Show("¡" + ctMBuscarCliente.Text + " no existe!");
            }
        }

    }
}
