﻿namespace Poryecto_ferreteria
{
    partial class AdminClientes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminClientes));
            this.ctClaveCliente = new System.Windows.Forms.TextBox();
            this.lblClave = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.ctNombreCliente = new System.Windows.Forms.TextBox();
            this.lblDireccion = new System.Windows.Forms.Label();
            this.ctDireccionCliente = new System.Windows.Forms.TextBox();
            this.lblFechaNacimientoCliente = new System.Windows.Forms.Label();
            this.dtpFechaNacimientoCliente = new System.Windows.Forms.DateTimePicker();
            this.lblRfc = new System.Windows.Forms.Label();
            this.ctRfc = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.buscarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ctMBuscarCliente = new System.Windows.Forms.ToolStripTextBox();
            this.MntNuevoClientes = new System.Windows.Forms.ToolStripMenuItem();
            this.MntGuardarClientes = new System.Windows.Forms.ToolStripMenuItem();
            this.MntEliminarClientes = new System.Windows.Forms.ToolStripMenuItem();
            this.btnAtras = new System.Windows.Forms.Button();
            this.cbCategoriaCliente = new System.Windows.Forms.ComboBox();
            this.lblCategoriaCliente = new System.Windows.Forms.Label();
            this.btnSalir = new System.Windows.Forms.Button();
            this.lbClientes = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbFiltros = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ctClaveCliente
            // 
            this.ctClaveCliente.Location = new System.Drawing.Point(89, 109);
            this.ctClaveCliente.Name = "ctClaveCliente";
            this.ctClaveCliente.Size = new System.Drawing.Size(56, 20);
            this.ctClaveCliente.TabIndex = 0;
            // 
            // lblClave
            // 
            this.lblClave.AutoSize = true;
            this.lblClave.Location = new System.Drawing.Point(30, 112);
            this.lblClave.Name = "lblClave";
            this.lblClave.Size = new System.Drawing.Size(37, 13);
            this.lblClave.TabIndex = 1;
            this.lblClave.Text = "Clave:";
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Location = new System.Drawing.Point(30, 151);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(47, 13);
            this.lblNombre.TabIndex = 2;
            this.lblNombre.Text = "Nombre:";
            // 
            // ctNombreCliente
            // 
            this.ctNombreCliente.Location = new System.Drawing.Point(89, 148);
            this.ctNombreCliente.Name = "ctNombreCliente";
            this.ctNombreCliente.Size = new System.Drawing.Size(408, 20);
            this.ctNombreCliente.TabIndex = 3;
            this.ctNombreCliente.TextChanged += new System.EventHandler(this.ctNombreCliente_TextChanged);
            // 
            // lblDireccion
            // 
            this.lblDireccion.AutoSize = true;
            this.lblDireccion.Location = new System.Drawing.Point(30, 189);
            this.lblDireccion.Name = "lblDireccion";
            this.lblDireccion.Size = new System.Drawing.Size(55, 13);
            this.lblDireccion.TabIndex = 4;
            this.lblDireccion.Text = "Dirección:";
            // 
            // ctDireccionCliente
            // 
            this.ctDireccionCliente.Location = new System.Drawing.Point(89, 186);
            this.ctDireccionCliente.Name = "ctDireccionCliente";
            this.ctDireccionCliente.Size = new System.Drawing.Size(408, 20);
            this.ctDireccionCliente.TabIndex = 5;
            this.ctDireccionCliente.TextChanged += new System.EventHandler(this.ctDireccionCliente_TextChanged_1);
            // 
            // lblFechaNacimientoCliente
            // 
            this.lblFechaNacimientoCliente.AutoSize = true;
            this.lblFechaNacimientoCliente.Location = new System.Drawing.Point(15, 237);
            this.lblFechaNacimientoCliente.Name = "lblFechaNacimientoCliente";
            this.lblFechaNacimientoCliente.Size = new System.Drawing.Size(109, 13);
            this.lblFechaNacimientoCliente.TabIndex = 6;
            this.lblFechaNacimientoCliente.Text = "Fecha de nacimiento:";
            // 
            // dtpFechaNacimientoCliente
            // 
            this.dtpFechaNacimientoCliente.Location = new System.Drawing.Point(130, 237);
            this.dtpFechaNacimientoCliente.Name = "dtpFechaNacimientoCliente";
            this.dtpFechaNacimientoCliente.Size = new System.Drawing.Size(200, 20);
            this.dtpFechaNacimientoCliente.TabIndex = 7;
            // 
            // lblRfc
            // 
            this.lblRfc.AutoSize = true;
            this.lblRfc.Location = new System.Drawing.Point(181, 112);
            this.lblRfc.Name = "lblRfc";
            this.lblRfc.Size = new System.Drawing.Size(31, 13);
            this.lblRfc.TabIndex = 8;
            this.lblRfc.Text = "RFC:";
            // 
            // ctRfc
            // 
            this.ctRfc.Location = new System.Drawing.Point(218, 109);
            this.ctRfc.Name = "ctRfc";
            this.ctRfc.Size = new System.Drawing.Size(199, 20);
            this.ctRfc.TabIndex = 9;
            this.ctRfc.Click += new System.EventHandler(this.ctRfc_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buscarToolStripMenuItem,
            this.ctMBuscarCliente,
            this.MntNuevoClientes,
            this.MntGuardarClientes,
            this.MntEliminarClientes});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(603, 27);
            this.menuStrip1.TabIndex = 10;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // buscarToolStripMenuItem
            // 
            this.buscarToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("buscarToolStripMenuItem.Image")));
            this.buscarToolStripMenuItem.Name = "buscarToolStripMenuItem";
            this.buscarToolStripMenuItem.Size = new System.Drawing.Size(70, 23);
            this.buscarToolStripMenuItem.Text = "Buscar";
            this.buscarToolStripMenuItem.Click += new System.EventHandler(this.buscarToolStripMenuItem_Click);
            // 
            // ctMBuscarCliente
            // 
            this.ctMBuscarCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ctMBuscarCliente.Name = "ctMBuscarCliente";
            this.ctMBuscarCliente.Size = new System.Drawing.Size(100, 23);
            // 
            // MntNuevoClientes
            // 
            this.MntNuevoClientes.Image = ((System.Drawing.Image)(resources.GetObject("MntNuevoClientes.Image")));
            this.MntNuevoClientes.Name = "MntNuevoClientes";
            this.MntNuevoClientes.Size = new System.Drawing.Size(70, 23);
            this.MntNuevoClientes.Text = "Nuevo";
            this.MntNuevoClientes.Click += new System.EventHandler(this.MntNuevoClientes_Click);
            // 
            // MntGuardarClientes
            // 
            this.MntGuardarClientes.Image = ((System.Drawing.Image)(resources.GetObject("MntGuardarClientes.Image")));
            this.MntGuardarClientes.Name = "MntGuardarClientes";
            this.MntGuardarClientes.Size = new System.Drawing.Size(77, 23);
            this.MntGuardarClientes.Text = "Guardar";
            this.MntGuardarClientes.Click += new System.EventHandler(this.MntGuardarClientes_Click);
            // 
            // MntEliminarClientes
            // 
            this.MntEliminarClientes.Image = ((System.Drawing.Image)(resources.GetObject("MntEliminarClientes.Image")));
            this.MntEliminarClientes.Name = "MntEliminarClientes";
            this.MntEliminarClientes.Size = new System.Drawing.Size(78, 23);
            this.MntEliminarClientes.Text = "Eliminar";
            this.MntEliminarClientes.Click += new System.EventHandler(this.MntEliminarClientes_Click);
            // 
            // btnAtras
            // 
            this.btnAtras.Image = ((System.Drawing.Image)(resources.GetObject("btnAtras.Image")));
            this.btnAtras.Location = new System.Drawing.Point(12, 30);
            this.btnAtras.Name = "btnAtras";
            this.btnAtras.Size = new System.Drawing.Size(37, 40);
            this.btnAtras.TabIndex = 11;
            this.btnAtras.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnAtras.UseVisualStyleBackColor = true;
            this.btnAtras.Click += new System.EventHandler(this.btnAtras_Click);
            // 
            // cbCategoriaCliente
            // 
            this.cbCategoriaCliente.FormattingEnabled = true;
            this.cbCategoriaCliente.Location = new System.Drawing.Point(424, 237);
            this.cbCategoriaCliente.Name = "cbCategoriaCliente";
            this.cbCategoriaCliente.Size = new System.Drawing.Size(145, 21);
            this.cbCategoriaCliente.TabIndex = 12;
            // 
            // lblCategoriaCliente
            // 
            this.lblCategoriaCliente.AutoSize = true;
            this.lblCategoriaCliente.Location = new System.Drawing.Point(362, 240);
            this.lblCategoriaCliente.Name = "lblCategoriaCliente";
            this.lblCategoriaCliente.Size = new System.Drawing.Size(55, 13);
            this.lblCategoriaCliente.TabIndex = 13;
            this.lblCategoriaCliente.Text = "Categoria:";
            // 
            // btnSalir
            // 
            this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
            this.btnSalir.Location = new System.Drawing.Point(554, 0);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(49, 48);
            this.btnSalir.TabIndex = 14;
            this.btnSalir.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // lbClientes
            // 
            this.lbClientes.FormattingEnabled = true;
            this.lbClientes.Location = new System.Drawing.Point(12, 295);
            this.lbClientes.Name = "lbClientes";
            this.lbClientes.Size = new System.Drawing.Size(384, 316);
            this.lbClientes.TabIndex = 15;
            this.lbClientes.SelectedIndexChanged += new System.EventHandler(this.lbClientes_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 276);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Lista de clientes";
            // 
            // cbFiltros
            // 
            this.cbFiltros.FormattingEnabled = true;
            this.cbFiltros.Location = new System.Drawing.Point(424, 349);
            this.cbFiltros.Name = "cbFiltros";
            this.cbFiltros.Size = new System.Drawing.Size(145, 21);
            this.cbFiltros.TabIndex = 17;
            this.cbFiltros.SelectedIndexChanged += new System.EventHandler(this.cbFiltros_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(421, 315);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(148, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Ordenar por tipo de categoria:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(60, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Atras";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(521, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Salir";
            // 
            // AdminClientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(603, 695);
            this.ControlBox = false;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbFiltros);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbClientes);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.lblCategoriaCliente);
            this.Controls.Add(this.cbCategoriaCliente);
            this.Controls.Add(this.btnAtras);
            this.Controls.Add(this.ctRfc);
            this.Controls.Add(this.lblRfc);
            this.Controls.Add(this.dtpFechaNacimientoCliente);
            this.Controls.Add(this.lblFechaNacimientoCliente);
            this.Controls.Add(this.ctDireccionCliente);
            this.Controls.Add(this.lblDireccion);
            this.Controls.Add(this.ctNombreCliente);
            this.Controls.Add(this.lblNombre);
            this.Controls.Add(this.lblClave);
            this.Controls.Add(this.ctClaveCliente);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AdminClientes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Administrar clientes";
            this.Load += new System.EventHandler(this.AdminClientes_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox ctClaveCliente;
        private System.Windows.Forms.Label lblClave;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.TextBox ctNombreCliente;
        private System.Windows.Forms.Label lblDireccion;
        private System.Windows.Forms.TextBox ctDireccionCliente;
        private System.Windows.Forms.Label lblFechaNacimientoCliente;
        private System.Windows.Forms.DateTimePicker dtpFechaNacimientoCliente;
        private System.Windows.Forms.Label lblRfc;
        private System.Windows.Forms.TextBox ctRfc;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem MntNuevoClientes;
        private System.Windows.Forms.ToolStripMenuItem MntGuardarClientes;
        private System.Windows.Forms.ToolStripMenuItem MntEliminarClientes;
        private System.Windows.Forms.Button btnAtras;
        private System.Windows.Forms.ComboBox cbCategoriaCliente;
        private System.Windows.Forms.Label lblCategoriaCliente;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.ToolStripMenuItem buscarToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox ctMBuscarCliente;
        private System.Windows.Forms.ListBox lbClientes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbFiltros;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}