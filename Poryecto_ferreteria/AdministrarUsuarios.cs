﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Poryecto_ferreteria.Login;

namespace Poryecto_ferreteria
{
    public partial class AdministrarUsuarios : Form
    {
        Usuarios usuario = new Usuarios();
        BRINDISEntities bd = new BRINDISEntities();
        public AdministrarUsuarios()
        {
            InitializeComponent();
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            this.Hide();
            Pantalla_principal pantalla_principal = new Pantalla_principal();
            pantalla_principal.Show(this);
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
            this.Close();
        }

        private void buscarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            buscar(ctMBuscarUsuario.Text.Trim());

        }

        private void nuevoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            eliminarToolStripMenuItem.Enabled = false;
            guardarToolStripMenuItem.Enabled = true;
            int maxAge = bd.Usuarios.Select(p => p.Usu_clave).DefaultIfEmpty(0).Max() + 1;
            limpiar();
            ctClaveUsuario.Text = maxAge.ToString();
        }
        void limpiar()
        {
            ctClaveUsuario.Text = "";
            ctContrasenaUsuario.Text = "";
            ctNombreUsuario.Text = "";
            rbAdmin.Checked = false;
            rbNormal.Checked = true;
        }

        private void guardarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int maxAge = bd.Usuarios.Select(p => p.Usu_clave).DefaultIfEmpty(0).Max();
            try
            {
                if (!ValidarCajasDeTextoCompletas())
                {
                    return;
                }

                if (Convert.ToInt32(ctClaveUsuario.Text) <= maxAge)
                {
                    Actualizar();
                    ActualizarListas();
                }
                else
                {
                    Guardar();
                    ActualizarListas();
                }
            }
            catch (Exception error)
            {

                MessageBox.Show(error.Message);
            }


        }
        void Guardar()
        {
            usuario.Usu_clave = Convert.ToInt32(ctClaveUsuario.Text.Trim());
            usuario.Usu_nombre = ctNombreUsuario.Text.Trim();
            usuario.Usu_contrasena = ctContrasenaUsuario.Text.Trim();
            if (rbAdmin.Checked)
            {
                usuario.Usu_tipo_id = 1;
            }
            else if (rbNormal.Checked)
            {
                usuario.Usu_tipo_id = 2;
            }
            bd.Usuarios.Add(usuario);
            bd.SaveChanges();
            MessageBox.Show("Usuario guardado correctamente");
        }
        void Eliminar()
        {

            if (ctClaveUsuario.Enabled == false)
            {
                if (Convert.ToInt32(ctClaveUsuario.Text) == DatosUsuario.SessionUsu.Usu_clave)
                {
                    usuario.Usu_clave = Convert.ToInt32(ctClaveUsuario.Text.Trim());
                    usuario.Usu_nombre = ctNombreUsuario.Text.Trim();
                    usuario.Usu_contrasena = ctContrasenaUsuario.Text.Trim();
                    if (rbAdmin.Checked)
                    {
                        usuario.Usu_tipo_id = 1;
                    }
                    else if (rbNormal.Checked)
                    {
                        usuario.Usu_tipo_id = 2;
                    }
                    bd.Usuarios.Remove(usuario);
                    bd.SaveChanges();

                    MessageBox.Show("Usuario eliminado correctamente");
                    Login login = new Login();
                    MatarSesion();
                    this.Hide();
                    login.Show();
                }
                usuario.Usu_clave = Convert.ToInt32(ctClaveUsuario.Text.Trim());
                usuario.Usu_nombre = ctNombreUsuario.Text.Trim();
                usuario.Usu_contrasena = ctContrasenaUsuario.Text.Trim();
                if (rbAdmin.Checked)
                {
                    usuario.Usu_tipo_id = 1;
                }
                else if (rbNormal.Checked)
                {
                    usuario.Usu_tipo_id = 2;
                }
                bd.Usuarios.Remove(usuario);
                bd.SaveChanges();

            }
            else
            {
                return;
            }

        }
        void Actualizar()
        {
            int usutipo = 0;
            usuario.Usu_clave = Convert.ToInt32(ctClaveUsuario.Text.Trim());
            usuario.Usu_nombre = ctNombreUsuario.Text.Trim();
            usuario.Usu_contrasena = ctContrasenaUsuario.Text.Trim();
            if (Convert.ToInt32(ctClaveUsuario.Text) == DatosUsuario.SessionUsu.Usu_clave)
            {
                if (rbAdmin.Checked)
                {
                    usuario.Usu_tipo_id = 1;
                }
                else if (rbNormal.Checked)
                {
                    DatosUsuario.TipoUsuario.tipo_nivel = 2;
                }
                bd.Usuarios.Remove(usuario);
                bd.SaveChanges();
                usuario.Usu_tipo_id = DatosUsuario.TipoUsuario.tipo_nivel;
                bd.Usuarios.Add(usuario);
                bd.SaveChanges();
                DatosUsuario.SessionUsu.Usu_nombre = usuario.Usu_nombre;
                MessageBox.Show("Usuario actualizado correctamente");
                if (rbNormal.Checked)
                {
                    MessageBox.Show("Ya no tienes acceso a esta pantalla");
                    this.Hide();
                    DatosUsuario.SessionUsu = usuario;
                    DatosUsuario.TipoUsuario.tipo_descripcion = "Normal";
                    Pantalla_principal pantalla_principal = new Pantalla_principal();
                    pantalla_principal.Show();
                }
            }
            else
            {
                if (rbAdmin.Checked)
                {
                    usuario.Usu_tipo_id = 1;
                    usutipo = usuario.Usu_tipo_id.Value;
                }
                else if (rbNormal.Checked)
                {
                    usuario.Usu_tipo_id = 2;
                    usutipo = usuario.Usu_tipo_id.Value;
                }
                bd.Usuarios.Remove(usuario);
                bd.SaveChanges();
                usuario.Usu_tipo_id = usutipo;
                bd.Usuarios.Add(usuario);
                bd.SaveChanges();
                MessageBox.Show("Usuario actualizado correctamente");
            }

        }

        private void eliminarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!ValidarCajasDeTextoCompletas())
            {
                return;
            }
            if (DatosUsuario.SessionUsu.Usu_clave == usuario.Usu_clave)
            {
                DialogResult result = MessageBox.Show("¿seguro desea elminarlo?", "El usuario que desea eliminar se esta utilizando", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (result == DialogResult.No)
                {
                    return;
                }
                else
                {
                    Eliminar();
                    ActualizarListas();
                }
            }
            Eliminar();
            ActualizarListas();

        }
        void MatarSesion()
        {
            DatosUsuario.SessionUsu = null;
            DatosUsuario.TipoUsuario = null;
        }

        private void AdministrarUsuarios_Load(object sender, EventArgs e)
        {
            guardarToolStripMenuItem.Enabled = false;
            eliminarToolStripMenuItem.Enabled = false;
            LLenarListaAdmin();
            LLenarListaNormal();
            ctClaveUsuario.Enabled = false;
            ValidacionesBasicasDeCaracteres();
        }
        private void ValidacionesBasicasDeCaracteres() 
        {
            ctMBuscarUsuario.MaxLength = 50;
            ctContrasenaUsuario.MaxLength = 50;
            ctContrasenaUsuario.MaxLength = 50;
        }
        bool ValidarCajasDeTextoCompletas() 
        {
            bool valContra = true;
            bool valNombre = true;
            bool valTipo = true;
            bool res = true;
            if (ctContrasenaUsuario.Text == "")
            {
                valNombre = false;
            }
            if (ctNombreUsuario.Text == "")
            {
                valContra = false;
            }
            if (rbAdmin.Checked == false && rbNormal.Checked == false)
            {
                valTipo = false;
            }
            if (validarCajasNoLLenas(valContra, valNombre, valTipo))
            {
                return false;
            }
            else {return true; }

        }
        private bool validarCajasNoLLenas(bool a,bool b, bool c) 
        {

            bool L = false;
            if (!a)
            {
                ctNombreUsuario.Text = "Por favor de llenar este campo";
                L = true;
            }
            if (!b)
            {
                ctContrasenaUsuario.Text = "Por favor de llenar este campo";
                L = true;
            }
            if (!c)
            {
                MessageBox.Show("Favor de elejir un tipo de usuario");
                L = true;
            }
            return L;
        }

        private void ctNombreUsuario_TextChanged(object sender, EventArgs e)
        {
            if (ctNombreUsuario.Text == "Por favor de llenar este campo")
            {
                ctNombreUsuario.Text = "";
            } 
        }

        private void ctContrasenaUsuario_TextChanged(object sender, EventArgs e)
        {
            if (ctContrasenaUsuario.Text == "Por favor de llenar este campo")
            {
                ctContrasenaUsuario.Text = "";
            }
        }
        void LLenarListaAdmin() 
        {
            List<Usuarios> UsuariosAdmin = new List<Usuarios>();
            foreach (var usu in bd.Usuarios)
            {
                if (usu.Usu_tipo_id == 2)
                {
                    UsuariosAdmin.Add(usu);
                }
            }
            foreach (var usu in UsuariosAdmin)
            {
                lbUsuariosAdmin.Items.Add(usu.Usu_nombre);
            }
        }
        void LLenarListaNormal() 
        {
            List<Usuarios> UsuariosNormal = new List<Usuarios>();
            foreach (var usu in bd.Usuarios)
            {
                if (usu.Usu_tipo_id == 1)
                {
                    UsuariosNormal.Add(usu);
                }
            }
            foreach (var usu in UsuariosNormal)
            {
                lbUsuariosNorm.Items.Add(usu.Usu_nombre);
            }
        }

        private void lbUsuariosNorm_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbUsuariosNorm.SelectedItem == null || lbUsuariosNorm.SelectedItem == "")
            {
                return;
            }
            buscar(lbUsuariosNorm.SelectedItem.ToString());
        }

        private void lbUsuariosAdmin_SelectedIndexChanged(object sender, EventArgs e)
        {
            buscar(lbUsuariosAdmin.SelectedItem.ToString());

        }
        void buscar(string nombreUsu) 
        {
            if (nombreUsu == "")
            {
                return;
            }
            usuario = bd.Usuarios.FirstOrDefault(x => x.Usu_nombre == nombreUsu);
            if (usuario != null)
            {

                ctClaveUsuario.Text = usuario.Usu_clave.ToString();
                ctContrasenaUsuario.Text = usuario.Usu_contrasena.Trim();
                ctNombreUsuario.Text = usuario.Usu_nombre.Trim();
                if (usuario.Usu_tipo_id == 1)
                {
                    rbAdmin.Checked = true;
                }
                else if (usuario.Usu_tipo_id == 2)
                {
                    rbNormal.Checked = true;
                }
            }
            guardarToolStripMenuItem.Enabled = true;
            eliminarToolStripMenuItem.Enabled = true;

        }
        void ActualizarListas()
        {
            BorrarListas();
            LLenarListaAdmin();
            LLenarListaNormal();
        }
        void BorrarListas() 
        {
            lbUsuariosAdmin.Items.Clear();
            lbUsuariosNorm.Items.Clear();
        }

    }
}
