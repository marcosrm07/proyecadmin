﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Poryecto_ferreteria
{
    public partial class Login : Form
    {
        BRINDISEntities bd = new BRINDISEntities();
        private bool nombreUsu;
        private bool contrasenaUsu;
        Usuarios Usuario_s = new Usuarios();
        public Login()
        {
            InitializeComponent();
        }

        private void btnIniciarSecion_Click(object sender, EventArgs e)
        {
            Pantalla_principal MenuPrincipal = new Pantalla_principal();
            if (!ValidarCajasDeTexto(ctNombreUsuario.Text, ctContrasena.Text))
            {
                return;
            }
            IniciarSesion(ctNombreUsuario.Text.Trim(), ctContrasena.Text.Trim(), Usuario_s);
            if (nombreUsu == true && contrasenaUsu == true)
            {
                definirTipoUsuario(DatosUsuario.SessionUsu);
                MenuPrincipal.Show();
                this.Hide();
                Limpiar();

            }
            else
            {
                if (!nombreUsu)
                {
                    ctNombreUsuario.Text = "Usuario no encontrado";
                }
                if (!contrasenaUsu)
                {
                    ctContrasena.Text = "Contraseña incorrecta";
                }
            }
        }
        private bool ValidarCajasDeTexto(string usunombre, string usucontra)
        {
            var r = true;
            if (usunombre == null || usunombre == "")
            {
                MessageBox.Show("Porfavor de ingresar el nombre correctamente para iniciar sesión");
                r = false;
            }
            if (usucontra == null || usucontra == "")
            {
                MessageBox.Show("Porfavor de ingresar el contraseña correctamente para iniciar sesión");
                r = false;
            }
            return r;
        }
        private void IniciarSesion(string UsuNombre, string UsuContraseña, Usuarios usuario)
        {
            usuario = bd.Usuarios.FirstOrDefault(x => x.Usu_nombre == UsuNombre);
            if (usuario == null)
            {
                MessageBox.Show("El usuario no existe: " + ctNombreUsuario.Text);
                return;
            }

            if (usuario.Usu_nombre.Trim() == UsuNombre)
            {
                nombreUsu = true;

                if (usuario.Usu_contrasena.Trim() == UsuContraseña)
                {
                    contrasenaUsu = true;
                    DatosUsuario.SessionUsu = usuario;

                }
                else
                {
                    contrasenaUsu = false;
                }
            }
            else
            {
                nombreUsu = false;
            }
        }
        public static class DatosUsuario
        {
            public static Usuarios SessionUsu;
            public static tipo TipoUsuario;
        }
        public void Limpiar()
        {
            ctNombreUsuario.Text = "";
            ctContrasena.Text = "";
        }
        void definirTipoUsuario(Usuarios usuario)
        {
            DatosUsuario.TipoUsuario = bd.tipo.Find(usuario.Usu_tipo_id);
        }

        private void Login_Load(object sender, EventArgs e)
        {
            ctNombreUsuario.MaxLength = 50;
            ctContrasena.MaxLength = 50;
            ctContrasena.UseSystemPasswordChar = true;
            pictureBox1.Image = Image.FromFile(Path.Combine(Application.StartupPath, @"imagenes\ferresoft.png"));
        }

        private void Mostrar_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
           
        }

        private void ctNombreUsuario_TextChanged(object sender, EventArgs e)
        {
            if (ctNombreUsuario.Text.Trim() == "Porfavor de ingresar el nombre correctamente para iniciar sesión" || ctNombreUsuario.Text.Trim() == "Usuario no encontrado")
            {
                ctNombreUsuario.Text = "";
            }
        }

        private void ctContrasena_TextChanged(object sender, EventArgs e)
        {
            if (ctContrasena.Text.Trim() == "Porfavor de ingresar el contraseña correctamente para iniciar sesión" || ctContrasena.Text.Trim() == "Contraseña incorrecta")
            {
                ctContrasena.Text = "";
            }
        }
    }
}
