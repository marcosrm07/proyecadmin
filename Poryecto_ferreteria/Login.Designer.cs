﻿namespace Poryecto_ferreteria
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PanelLogin = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lbContrasena = new System.Windows.Forms.Label();
            this.lbNombre = new System.Windows.Forms.Label();
            this.btnIniciarSecion = new System.Windows.Forms.Button();
            this.ctContrasena = new System.Windows.Forms.TextBox();
            this.ctNombreUsuario = new System.Windows.Forms.TextBox();
            this.PanelLogin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelLogin
            // 
            this.PanelLogin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelLogin.Controls.Add(this.pictureBox1);
            this.PanelLogin.Controls.Add(this.lbContrasena);
            this.PanelLogin.Controls.Add(this.lbNombre);
            this.PanelLogin.Controls.Add(this.btnIniciarSecion);
            this.PanelLogin.Controls.Add(this.ctContrasena);
            this.PanelLogin.Controls.Add(this.ctNombreUsuario);
            this.PanelLogin.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.PanelLogin.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.PanelLogin.Location = new System.Drawing.Point(30, 12);
            this.PanelLogin.Name = "PanelLogin";
            this.PanelLogin.Size = new System.Drawing.Size(667, 414);
            this.PanelLogin.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(216, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(270, 222);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // lbContrasena
            // 
            this.lbContrasena.AutoSize = true;
            this.lbContrasena.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbContrasena.Location = new System.Drawing.Point(257, 294);
            this.lbContrasena.Name = "lbContrasena";
            this.lbContrasena.Size = new System.Drawing.Size(163, 31);
            this.lbContrasena.TabIndex = 5;
            this.lbContrasena.Text = "Contraseña:";
            // 
            // lbNombre
            // 
            this.lbNombre.AutoSize = true;
            this.lbNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNombre.Location = new System.Drawing.Point(275, 228);
            this.lbNombre.Name = "lbNombre";
            this.lbNombre.Size = new System.Drawing.Size(118, 31);
            this.lbNombre.TabIndex = 4;
            this.lbNombre.Text = "Nombre:";
            // 
            // btnIniciarSecion
            // 
            this.btnIniciarSecion.Location = new System.Drawing.Point(244, 370);
            this.btnIniciarSecion.Name = "btnIniciarSecion";
            this.btnIniciarSecion.Size = new System.Drawing.Size(187, 36);
            this.btnIniciarSecion.TabIndex = 2;
            this.btnIniciarSecion.Text = "Entrar";
            this.btnIniciarSecion.UseVisualStyleBackColor = true;
            this.btnIniciarSecion.Click += new System.EventHandler(this.btnIniciarSecion_Click);
            // 
            // ctContrasena
            // 
            this.ctContrasena.Location = new System.Drawing.Point(202, 328);
            this.ctContrasena.Name = "ctContrasena";
            this.ctContrasena.PasswordChar = '*';
            this.ctContrasena.Size = new System.Drawing.Size(270, 20);
            this.ctContrasena.TabIndex = 1;
            this.ctContrasena.TextChanged += new System.EventHandler(this.ctContrasena_TextChanged);
            // 
            // ctNombreUsuario
            // 
            this.ctNombreUsuario.Location = new System.Drawing.Point(202, 262);
            this.ctNombreUsuario.Name = "ctNombreUsuario";
            this.ctNombreUsuario.Size = new System.Drawing.Size(270, 20);
            this.ctNombreUsuario.TabIndex = 0;
            this.ctNombreUsuario.TextChanged += new System.EventHandler(this.ctNombreUsuario_TextChanged);
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(728, 438);
            this.Controls.Add(this.PanelLogin);
            this.MaximizeBox = false;
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Iniciar Sesión";
            this.Load += new System.EventHandler(this.Login_Load);
            this.PanelLogin.ResumeLayout(false);
            this.PanelLogin.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PanelLogin;
        private System.Windows.Forms.Label lbContrasena;
        private System.Windows.Forms.Label lbNombre;
        private System.Windows.Forms.Button btnIniciarSecion;
        private System.Windows.Forms.TextBox ctContrasena;
        private System.Windows.Forms.TextBox ctNombreUsuario;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}