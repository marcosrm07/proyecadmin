﻿namespace Poryecto_ferreteria
{
    partial class Pantalla_principal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Pantalla_principal));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.pbImagenUsuario = new System.Windows.Forms.PictureBox();
            this.ctTipoUsuarioAct = new System.Windows.Forms.TextBox();
            this.ctNombreUsuarioAct = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.MntAdminClientes = new System.Windows.Forms.ToolStripMenuItem();
            this.MntAdminCategorias = new System.Windows.Forms.ToolStripMenuItem();
            this.MntAdminUsuarios = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSalir = new System.Windows.Forms.Button();
            this.btnCerrarSesion = new System.Windows.Forms.Button();
            this.entityCommand1 = new System.Data.Entity.Core.EntityClient.EntityCommand();
            this.entityCommand2 = new System.Data.Entity.Core.EntityClient.EntityCommand();
            this.entityCommand3 = new System.Data.Entity.Core.EntityClient.EntityCommand();
            this.entityCommand4 = new System.Data.Entity.Core.EntityClient.EntityCommand();
            this.entityCommand5 = new System.Data.Entity.Core.EntityClient.EntityCommand();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbImagenUsuario)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pbImagenUsuario);
            this.panel1.Controls.Add(this.ctTipoUsuarioAct);
            this.panel1.Controls.Add(this.ctNombreUsuarioAct);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(12, 81);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(648, 281);
            this.panel1.TabIndex = 1;
            // 
            // pbImagenUsuario
            // 
            this.pbImagenUsuario.BackColor = System.Drawing.Color.Transparent;
            this.pbImagenUsuario.Location = new System.Drawing.Point(21, 28);
            this.pbImagenUsuario.Name = "pbImagenUsuario";
            this.pbImagenUsuario.Size = new System.Drawing.Size(239, 211);
            this.pbImagenUsuario.TabIndex = 7;
            this.pbImagenUsuario.TabStop = false;
            // 
            // ctTipoUsuarioAct
            // 
            this.ctTipoUsuarioAct.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ctTipoUsuarioAct.Enabled = false;
            this.ctTipoUsuarioAct.Location = new System.Drawing.Point(355, 98);
            this.ctTipoUsuarioAct.Name = "ctTipoUsuarioAct";
            this.ctTipoUsuarioAct.ReadOnly = true;
            this.ctTipoUsuarioAct.Size = new System.Drawing.Size(117, 20);
            this.ctTipoUsuarioAct.TabIndex = 6;
            // 
            // ctNombreUsuarioAct
            // 
            this.ctNombreUsuarioAct.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ctNombreUsuarioAct.Enabled = false;
            this.ctNombreUsuarioAct.Location = new System.Drawing.Point(355, 50);
            this.ctNombreUsuarioAct.Name = "ctNombreUsuarioAct";
            this.ctNombreUsuarioAct.ReadOnly = true;
            this.ctNombreUsuarioAct.Size = new System.Drawing.Size(290, 20);
            this.ctNombreUsuarioAct.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(266, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nombre:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(266, 101);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Tipo de usuario:";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.DodgerBlue;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MntAdminClientes,
            this.MntAdminCategorias,
            this.MntAdminUsuarios});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(684, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // MntAdminClientes
            // 
            this.MntAdminClientes.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MntAdminClientes.Name = "MntAdminClientes";
            this.MntAdminClientes.Size = new System.Drawing.Size(131, 20);
            this.MntAdminClientes.Text = "Administrar Clientes";
            this.MntAdminClientes.Click += new System.EventHandler(this.MntAdminClientes_Click);
            // 
            // MntAdminCategorias
            // 
            this.MntAdminCategorias.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MntAdminCategorias.Name = "MntAdminCategorias";
            this.MntAdminCategorias.Size = new System.Drawing.Size(145, 20);
            this.MntAdminCategorias.Text = "Administrar Categorias";
            this.MntAdminCategorias.Click += new System.EventHandler(this.MntAdminCategorias_Click);
            // 
            // MntAdminUsuarios
            // 
            this.MntAdminUsuarios.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MntAdminUsuarios.Name = "MntAdminUsuarios";
            this.MntAdminUsuarios.Size = new System.Drawing.Size(134, 20);
            this.MntAdminUsuarios.Text = "Administrar Usuarios";
            this.MntAdminUsuarios.Click += new System.EventHandler(this.MntAdminUsuarios_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
            this.btnSalir.Location = new System.Drawing.Point(637, 0);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(47, 47);
            this.btnSalir.TabIndex = 3;
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnCerrarSesion
            // 
            this.btnCerrarSesion.Image = ((System.Drawing.Image)(resources.GetObject("btnCerrarSesion.Image")));
            this.btnCerrarSesion.Location = new System.Drawing.Point(12, 36);
            this.btnCerrarSesion.Name = "btnCerrarSesion";
            this.btnCerrarSesion.Size = new System.Drawing.Size(43, 39);
            this.btnCerrarSesion.TabIndex = 4;
            this.btnCerrarSesion.UseVisualStyleBackColor = true;
            this.btnCerrarSesion.Click += new System.EventHandler(this.btnCerrarSesion_Click);
            // 
            // entityCommand1
            // 
            this.entityCommand1.CommandTimeout = 0;
            this.entityCommand1.CommandTree = null;
            this.entityCommand1.Connection = null;
            this.entityCommand1.EnablePlanCaching = true;
            this.entityCommand1.Transaction = null;
            // 
            // entityCommand2
            // 
            this.entityCommand2.CommandTimeout = 0;
            this.entityCommand2.CommandTree = null;
            this.entityCommand2.Connection = null;
            this.entityCommand2.EnablePlanCaching = true;
            this.entityCommand2.Transaction = null;
            // 
            // entityCommand3
            // 
            this.entityCommand3.CommandTimeout = 0;
            this.entityCommand3.CommandTree = null;
            this.entityCommand3.Connection = null;
            this.entityCommand3.EnablePlanCaching = true;
            this.entityCommand3.Transaction = null;
            // 
            // entityCommand4
            // 
            this.entityCommand4.CommandTimeout = 0;
            this.entityCommand4.CommandTree = null;
            this.entityCommand4.Connection = null;
            this.entityCommand4.EnablePlanCaching = true;
            this.entityCommand4.Transaction = null;
            // 
            // entityCommand5
            // 
            this.entityCommand5.CommandTimeout = 0;
            this.entityCommand5.CommandTree = null;
            this.entityCommand5.Connection = null;
            this.entityCommand5.EnablePlanCaching = true;
            this.entityCommand5.Transaction = null;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(61, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Cerrar sesion";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(604, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Salir";
            // 
            // Pantalla_principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 393);
            this.ControlBox = false;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnCerrarSesion);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Pantalla_principal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu Principal";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbImagenUsuario)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem MntAdminClientes;
        private System.Windows.Forms.ToolStripMenuItem MntAdminCategorias;
        private System.Windows.Forms.ToolStripMenuItem MntAdminUsuarios;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.TextBox ctTipoUsuarioAct;
        private System.Windows.Forms.TextBox ctNombreUsuarioAct;
        private System.Windows.Forms.Button btnCerrarSesion;
        private System.Data.Entity.Core.EntityClient.EntityCommand entityCommand1;
        private System.Data.Entity.Core.EntityClient.EntityCommand entityCommand2;
        private System.Data.Entity.Core.EntityClient.EntityCommand entityCommand3;
        private System.Data.Entity.Core.EntityClient.EntityCommand entityCommand4;
        private System.Data.Entity.Core.EntityClient.EntityCommand entityCommand5;
        private System.Windows.Forms.PictureBox pbImagenUsuario;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}

