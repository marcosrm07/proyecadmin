﻿namespace Poryecto_ferreteria
{
    partial class AdminCategorias
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminCategorias));
            this.lblClaveCategoria = new System.Windows.Forms.Label();
            this.lblDescripcionCategoria = new System.Windows.Forms.Label();
            this.ctClaveCategoria = new System.Windows.Forms.TextBox();
            this.ctDescripcionCategoria = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.buscarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ctMBuscar = new System.Windows.Forms.ToolStripComboBox();
            this.MntNuevoCategoria = new System.Windows.Forms.ToolStripMenuItem();
            this.MntGuardarCategoria = new System.Windows.Forms.ToolStripMenuItem();
            this.MntEliminarCategoria = new System.Windows.Forms.ToolStripMenuItem();
            this.btnAtras = new System.Windows.Forms.Button();
            this.btnSalir = new System.Windows.Forms.Button();
            this.lbCategorias = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblClaveCategoria
            // 
            this.lblClaveCategoria.AutoSize = true;
            this.lblClaveCategoria.Location = new System.Drawing.Point(28, 71);
            this.lblClaveCategoria.Name = "lblClaveCategoria";
            this.lblClaveCategoria.Size = new System.Drawing.Size(37, 13);
            this.lblClaveCategoria.TabIndex = 0;
            this.lblClaveCategoria.Text = "Clave:";
            // 
            // lblDescripcionCategoria
            // 
            this.lblDescripcionCategoria.AutoSize = true;
            this.lblDescripcionCategoria.Location = new System.Drawing.Point(28, 124);
            this.lblDescripcionCategoria.Name = "lblDescripcionCategoria";
            this.lblDescripcionCategoria.Size = new System.Drawing.Size(66, 13);
            this.lblDescripcionCategoria.TabIndex = 1;
            this.lblDescripcionCategoria.Text = "Descripción:";
            // 
            // ctClaveCategoria
            // 
            this.ctClaveCategoria.Location = new System.Drawing.Point(31, 87);
            this.ctClaveCategoria.Name = "ctClaveCategoria";
            this.ctClaveCategoria.Size = new System.Drawing.Size(100, 20);
            this.ctClaveCategoria.TabIndex = 2;
            // 
            // ctDescripcionCategoria
            // 
            this.ctDescripcionCategoria.Location = new System.Drawing.Point(31, 140);
            this.ctDescripcionCategoria.Multiline = true;
            this.ctDescripcionCategoria.Name = "ctDescripcionCategoria";
            this.ctDescripcionCategoria.Size = new System.Drawing.Size(310, 111);
            this.ctDescripcionCategoria.TabIndex = 3;
            this.ctDescripcionCategoria.TextChanged += new System.EventHandler(this.ctDescripcionCategoria_TextChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buscarToolStripMenuItem,
            this.ctMBuscar,
            this.MntNuevoCategoria,
            this.MntGuardarCategoria,
            this.MntEliminarCategoria});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(597, 27);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // buscarToolStripMenuItem
            // 
            this.buscarToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("buscarToolStripMenuItem.Image")));
            this.buscarToolStripMenuItem.Name = "buscarToolStripMenuItem";
            this.buscarToolStripMenuItem.Size = new System.Drawing.Size(70, 23);
            this.buscarToolStripMenuItem.Text = "Buscar";
            this.buscarToolStripMenuItem.Click += new System.EventHandler(this.buscarToolStripMenuItem_Click);
            // 
            // ctMBuscar
            // 
            this.ctMBuscar.Name = "ctMBuscar";
            this.ctMBuscar.Size = new System.Drawing.Size(121, 23);
            // 
            // MntNuevoCategoria
            // 
            this.MntNuevoCategoria.Image = ((System.Drawing.Image)(resources.GetObject("MntNuevoCategoria.Image")));
            this.MntNuevoCategoria.Name = "MntNuevoCategoria";
            this.MntNuevoCategoria.Size = new System.Drawing.Size(70, 23);
            this.MntNuevoCategoria.Text = "Nuevo";
            this.MntNuevoCategoria.Click += new System.EventHandler(this.MntNuevoCategoria_Click);
            // 
            // MntGuardarCategoria
            // 
            this.MntGuardarCategoria.Image = ((System.Drawing.Image)(resources.GetObject("MntGuardarCategoria.Image")));
            this.MntGuardarCategoria.Name = "MntGuardarCategoria";
            this.MntGuardarCategoria.Size = new System.Drawing.Size(77, 23);
            this.MntGuardarCategoria.Text = "Guardar";
            this.MntGuardarCategoria.Click += new System.EventHandler(this.MntGuardarCategoria_Click);
            // 
            // MntEliminarCategoria
            // 
            this.MntEliminarCategoria.Image = ((System.Drawing.Image)(resources.GetObject("MntEliminarCategoria.Image")));
            this.MntEliminarCategoria.Name = "MntEliminarCategoria";
            this.MntEliminarCategoria.Size = new System.Drawing.Size(78, 23);
            this.MntEliminarCategoria.Text = "Eliminar";
            this.MntEliminarCategoria.Click += new System.EventHandler(this.MntEliminarCategoria_Click);
            // 
            // btnAtras
            // 
            this.btnAtras.Image = ((System.Drawing.Image)(resources.GetObject("btnAtras.Image")));
            this.btnAtras.Location = new System.Drawing.Point(12, 30);
            this.btnAtras.Name = "btnAtras";
            this.btnAtras.Size = new System.Drawing.Size(41, 38);
            this.btnAtras.TabIndex = 5;
            this.btnAtras.UseVisualStyleBackColor = true;
            this.btnAtras.Click += new System.EventHandler(this.btnAtras_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Image = ((System.Drawing.Image)(resources.GetObject("btnSalir.Image")));
            this.btnSalir.Location = new System.Drawing.Point(547, 1);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(50, 51);
            this.btnSalir.TabIndex = 6;
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // lbCategorias
            // 
            this.lbCategorias.FormattingEnabled = true;
            this.lbCategorias.Location = new System.Drawing.Point(443, 71);
            this.lbCategorias.Name = "lbCategorias";
            this.lbCategorias.Size = new System.Drawing.Size(120, 186);
            this.lbCategorias.TabIndex = 7;
            this.lbCategorias.SelectedIndexChanged += new System.EventHandler(this.lbCategorias_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(440, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Lista de categorias activas";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(63, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Atras";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(514, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Salir";
            // 
            // AdminCategorias
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(597, 263);
            this.ControlBox = false;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbCategorias);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btnAtras);
            this.Controls.Add(this.ctDescripcionCategoria);
            this.Controls.Add(this.ctClaveCategoria);
            this.Controls.Add(this.lblDescripcionCategoria);
            this.Controls.Add(this.lblClaveCategoria);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AdminCategorias";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Administrar Categorias";
            this.Load += new System.EventHandler(this.AdminCategorias_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblClaveCategoria;
        private System.Windows.Forms.Label lblDescripcionCategoria;
        private System.Windows.Forms.TextBox ctClaveCategoria;
        private System.Windows.Forms.TextBox ctDescripcionCategoria;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem MntNuevoCategoria;
        private System.Windows.Forms.ToolStripMenuItem MntGuardarCategoria;
        private System.Windows.Forms.ToolStripMenuItem MntEliminarCategoria;
        private System.Windows.Forms.Button btnAtras;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.ToolStripMenuItem buscarToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox ctMBuscar;
        private System.Windows.Forms.ListBox lbCategorias;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
    }
}