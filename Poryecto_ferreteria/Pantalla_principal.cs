﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Poryecto_ferreteria.Login;

namespace Poryecto_ferreteria
{
    public partial class Pantalla_principal : Form
    {
        Login login = new Login();
        BRINDISEntities bd = new BRINDISEntities();
        public Pantalla_principal()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Pantalla_principal pantalla_principal = new Pantalla_principal();
            if (DatosUsuario.TipoUsuario.tipo_nivel == 1)
            {
                pbImagenUsuario.Image = null;
                MntAdminUsuarios.Enabled = true;
                pbImagenUsuario.Image = Image.FromFile(Path.Combine(Application.StartupPath, @"imagenes\usuario_admin.jfif"));
                pantalla_principal.Icon = new Icon(Path.Combine(Application.StartupPath, @"imagenes\iconos\usuario_admin.ico"));

            }
            else if(DatosUsuario.TipoUsuario.tipo_nivel == 2)
            {
                pbImagenUsuario.Image = null;
                MntAdminUsuarios.Enabled = false;
                pbImagenUsuario.Image = Image.FromFile(Path.Combine(Application.StartupPath, @"imagenes\usuario_normal.jfif"));
                pantalla_principal.Icon = new Icon(Path.Combine(Application.StartupPath, @"imagenes\iconos\usuario_normal.ico"));
            }


            llenarDatosUsuario();
        }
        void llenarDatosUsuario() 
        {
            ctNombreUsuarioAct.Text = DatosUsuario.SessionUsu.Usu_nombre.Trim();
            ctTipoUsuarioAct.Text = Convert.ToString(DatosUsuario.TipoUsuario.tipo_descripcion.Trim());

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
          DialogResult result = MessageBox.Show("Desea salir del programa?","Cerrar sesión",MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                this.Close();
            }

        }

        private void btnCerrarSesion_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Desea cerrar sesion actual?", "Cerrar sesión", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                this.Hide();
                login.Show(this);
                DatosUsuario.SessionUsu = null;
                DatosUsuario.TipoUsuario = null;
            }
        }

        private void MntAdminClientes_Click(object sender, EventArgs e)
        {
            AdminClientes adminClientes = new AdminClientes();
            this.Hide();
            adminClientes.Show(this);
        }

        private void MntAdminCategorias_Click(object sender, EventArgs e)
        {
            AdminCategorias adminCategorias = new AdminCategorias();
            this.Hide();
            adminCategorias.Show(this);
        }

        private void MntAdminUsuarios_Click(object sender, EventArgs e)
        {
            this.Hide();
            AdministrarUsuarios administrarUsuarios = new AdministrarUsuarios();
            administrarUsuarios.Show(this);
        }
    

    }
}
